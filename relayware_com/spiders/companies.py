# -*- coding: utf-8 -*-
import re
import csv
import json
import scrapy

from scrapy.exceptions import CloseSpider
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode


class CompaniesSpider(scrapy.Spider):
    name = 'companies'
    allowed_domains = ['relayware.com']

    search_url = 'https://nutanix.portal.relayware.com/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=relayLocatorWS&methodName=getDisplayResults&locatorid=1&returnFormat=json&countryID=17&location={}&distance=20&selectallResellerAuditTier=ResellerAuditTier&flagIDList=6507&flagIDList=6506&flagIDList=6505&flagIDList=6508&flagIDList=7446&flagIDList=7448'
    count = {}

    def start_requests(self):
        if 'ZIPCODES' not in self.settings:
            raise CloseSpider("You must specificy ZIPCODES file in settings.py")

        with open(self.settings['ZIPCODES'], encoding='utf8') as csvfile:
            dr = csv.DictReader(csvfile)
            next(dr) # skip header
            for line in dr:
                code = line['Zip Code']
                url = self.search_url.format(code)
                meta = {'code': code}

                yield scrapy.Request(url=url, meta=meta, callback=self.parse)


    def parse(self, response):
        data = json.loads(response.text)
        search_response = response.replace(body=str.encode(data['list']))
        profile_urls = [response.urljoin(url) for url in search_response.css('.companyProfileRow .viewProfileTD a.viewProfileLink ::attr(href)').extract()]

        current = len(profile_urls)

        if not current:
            return

        code = response.meta['code']
        meta = {'code': code}

        for url in profile_urls:
            yield scrapy.Request(url=url, meta=meta, callback=self.parse_profile)

        total = data['HASRESULTS']

        if code not in self.count:
            self.count[code] = (total, current)
        else:
            self.count[code][1] += current

        if self.count[code][0] > self.count[code][1]:
            url = self.get_next_url(response, start_from=self.count[code][1]+1)
            if url:
                yield scrapy.Request(url=url, meta=meta, callback=self.parse)


    def parse_profile(self, response):
        d = {}

        d['Profile URL'] = response.url
        d['Partner'] = {}
        d['Locator'] = {}

        d['Partner']['Name'] = response.css('#organisationName ::text').extract_first()
        if d['Partner']['Name']:
            d['Partner']['Name'] = d['Partner']['Name'].strip()

        tab1 = response.xpath('//*[@id="partnerLocatorTab1"]//*[@id="relayFormDisplay"]')

        d['Partner']['Website URL'] = tab1.xpath('./*[contains(@class, "form-group") and descendant::label[contains(text(), "Website URL")]]//input/@value').extract_first()
        d['Partner']['Telephone'] = tab1.xpath('./*[contains(@class, "form-group") and descendant::label[contains(text(), "Telephone")]]//input/@value').extract_first()
        d['Partner']['Partner Tier'] = tab1.xpath('./*[contains(@class, "form-group") and descendant::label[contains(text(), "Partner Tier")]]//select/option[@selected="SELECTED"]/text()').extract_first()
        d['Partner']['Address'] = '\n'.join(filter(None, [t.strip() for t in tab1.xpath('./*[contains(@class, "form-group")]//input[contains(@id, "location_Address1") or contains(@id, "location_Address2") or contains(@id, "location_Address3")]/@value').extract()]))
        d['Partner']['Town'] = tab1.xpath('./*[contains(@class, "form-group") and descendant::label[contains(text(), "Town")]]//input/@value').extract_first()
        d['Partner']['State'] = tab1.xpath('./*[contains(@class, "form-group") and descendant::label[contains(text(), "State/County")]]//input/@value').extract_first()
        d['Partner']['Zip'] = tab1.xpath('./*[contains(@class, "form-group") and descendant::label[contains(text(), "Zip/Postal Code")]]//input/@value').extract_first()
        d['Partner']['Country'] = tab1.xpath('./*[contains(@class, "form-group") and descendant::label[contains(text(), "Country")]]//select/option[@selected="SELECTED"]/text()').extract_first()
        d['Partner']['Primary Contact'] = tab1.xpath('./*[contains(@class, "form-group") and descendant::label[contains(text(), "Primary Contact")]]//input/@value').extract_first()

        tab2 = response.xpath('//*[@id="partnerLocatorTab2"]//*[@id="relayFormDisplay"]')

        d['Locator']['Primary Contact'] = tab2.xpath('./*[contains(@class, "form-group")]//input[contains(@id, "Integer_3304")]/@value').extract_first()
        d['Locator']['Account Manager'] = tab2.xpath('./*[contains(@class, "form-group")]//input[contains(@id, "Integer_2231")]/@value').extract_first()
        d['Locator']['Elected Distributor'] = tab2.xpath('./*[contains(@class, "form-group")]//input[contains(@id, "Integer_3135")]/@value').extract_first()
        d['Locator']['Key Contacts'] = {}
        d['Locator']['Key Contacts']['Primary Contact'] = tab2.xpath('./*[contains(@class, "form-group")]//input[contains(@id, "Integer_1750")]/@value').extract_first()
        d['Locator']['Key Contacts']['Sales Manager'] = tab2.xpath('./*[contains(@class, "form-group")]//input[contains(@id, "Integer_2028")]/@value').extract_first()
        d['Locator']['Key Contacts']['Training Manager'] = tab2.xpath('./*[contains(@class, "form-group")]//input[contains(@id, "Integer_2197")]/@value').extract_first()
        d['Locator']['Description'] = tab2.xpath('./*[contains(@class, "form-group")]//input[contains(@id, "Text_2601")]/@value').extract_first()

        return d


    def get_next_url(self, response, start_from=0):
        request_url = response.url
        next_url = None

        p = list(urlparse(request_url))
        q = dict(parse_qsl(p[4]))

        try:
            q['getResultsFrom'] = start_from

            p[4] = urlencode(q)
            next_url = urlunparse(p)
        except:
            self.logger.warning('Unable to parse url: <{}>'.format(request_url))

        return next_url
